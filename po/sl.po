# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Nejc Bertoncelj <nbert@siol.net>, 2020.
# Martin Srebotnjak <miles@filmsi.net>, 2024.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-24 17:04-0400\n"
"PO-Revision-Date: 2024-08-04 19:39+0000\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <https://translate.fedoraproject.org/projects/"
"pulseaudio/pavucontrol/sl/>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || "
"n%100==4 ? 2 : 3;\n"
"X-Generator: Weblate 5.6.2\n"

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:7
#: src/org.pulseaudio.pavucontrol.desktop.in:5 src/mainwindow.ui:5
msgid "Volume Control"
msgstr "Nastavitev glasnosti"

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:8
msgid "Adjust device and app volumes"
msgstr "Prilagodite glasnost naprave in programa"

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:9
#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:11
msgid "The PulseAudio Developers"
msgstr "Razvijalci PulseAudio"

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:14
msgid ""
"PulseAudio Volume Control (pavucontrol) is a volume control tool (“mixer”) "
"for the PulseAudio sound server. In contrast to classic mixer tools, this "
"one allows you to control both the volume of hardware devices and of each "
"playback stream separately."
msgstr ""
"Nadzor glasnosti Pulse Audio (pavucontrol, angl. PulseAudio Volume Control) "
"je orodje za nadzor glasnosti (\"mešalnik\") za zvokovni strežnik "
"PulseAudio. Za razliko od klasičnih orodij za mešanje to omogoča nadzor tako "
"glasnosti strojnih naprav kot tudi vsakega toka predvajanja posebej."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:21
msgid "The “Playback” tab"
msgstr "Zavihek \"Predvajanje\""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:25
msgid "The “Recording” tab"
msgstr "Zavihek \"Snemanje\""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:29
msgid "The “Output” tab"
msgstr "Zavihek \"Izhod\""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:33
msgid "The “Input” tab"
msgstr "Zavihek \"Vhod\""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:37
msgid "The “Configuration” tab"
msgstr "Zavihek \"Nastavitve\""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:45
msgid "Migrate from Gtk 3 to 4."
msgstr "Migracija iz Gtk 3 na 4."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:46
msgid "Embed UI resources in executable."
msgstr "Vdelani viri vmesnika v izvršljivi datoteki programa."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:47
msgid "Rename \"Set as fallback\" to \"Default\" for better legibility."
msgstr ""
"Preimenovano \"Nastavi kot obnovitveno\" v \"privzeto\", za boljšo "
"berljivost."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:48
msgid "Support 144 Hz monitors with level bars."
msgstr "Podpora za 144 Hz zaslone z stolpci glasnosti."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:49
msgid ""
"App icons will fallback to generic rather than missing image, and this will "
"be more common with Gtk 4."
msgstr ""
"Ikone programa se povrnejo v splošne namesto manjkajoče slike, kar bo "
"pogosteje v rabi z Gtk 4."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:51
msgid "Lots of translation updates."
msgstr "Veliko posodobitev prevodov."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:52
msgid "Drop autotools build in favour of meson."
msgstr "Opuščena gradnja autotools, namesto tega uporabljen meson."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:53
msgid "Make libcanberra dependency optional."
msgstr "Odvisnost libcanberra je neobvezna."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:60
msgid "Support for switching Bluetooth codecs (new in PulseAudio 15.0)."
msgstr "Podpora za preklapljanje kodekov Bluetooth (novo v PulseAudio 15.0)."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:61
msgid ""
"Support for locking card profiles (new in PulseAudio 15.0). Locking a "
"profile prevents PulseAudio from automatically switching away from that "
"profile on plug/unplug events."
msgstr ""
"Podpora za zaklep profilov kartic (novo v PulseAudio 15.0). Zaklep profila "
"prepreči, da bi PulseAudio samodejno preklopil s tega profila ob dogodkih "
"vklopa/izklopa naprav."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:62
msgid ""
"New translations: Asturian, Basque, Belarusian, Galician, Hebrew, Kazakh, "
"Norwegian Bokmål, Sinhala, Slovenian"
msgstr ""
"Novi prevodi: asturijski, baskovski, beloruski, galicijski, hebrejski, "
"kazaški, norveški Bokmal, sinhalski, slovenski"

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:63
msgid ""
"Updated translations: Catalan, Chinese (Simplified), Chinese (Traditional), "
"Croatian, Danish, Dutch, Finnish, French, German, Hungarian, Italian, "
"Japanese, Korean, Lithuanian, Norwegian Nynorsk, Polish, Portugese, "
"Portugese (Brazil), Slovak, Spanish, Swedish, Turkish, Ukrainian."
msgstr ""
"Posodobljeni prevodi: katalonski, kitajski (poenostavljeni), kitajski "
"(tradicionalni), hrvaški, danski, nizozemski, finski, francoski, nemški, "
"madžarski, italijanski, japonski, korejski, litvanski, norveški Nynorsk, "
"poljski, portugalski, portugalski (Brazilija), slovaški, španski, švedski, "
"turški, ukrajinski."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:70
msgid ""
"There can now be only one pavucontrol window open at a time. Trying to start "
"pavucontrol for a second time brings the first window to foreground."
msgstr ""
"Zdaj je lahko naenkrat odprto le eno okno pavucontrol. Poskusi še drugega "
"zagona pavucontrol postavi prvo okno v ospredje."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:71
msgid ""
"Added a \"Show volume meters\" checkbox to the Configuration tab. Disabling "
"the volume meters reduces CPU use."
msgstr ""
"Dodano potrditveno polje \"Pokaži merilnike glasnosti\" v zavihku "
"Nastavitev. Onemogočanje merilnikov glasnosti zmanjša rabo CPE."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:72
msgid "Improve the use of space (remove useless margins and paddings)."
msgstr "Izboljšana raba prostora (odstranjeni odvečni robovi in blazinjenja)."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:73
msgid "Use a more appropriate icon for the channel lock button."
msgstr "Uporabljena primernejša ikona za gumb zaklepa kanala."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:74
msgid ""
"Better channel label layout, prevents volume sliders from getting unaligned."
msgstr ""
"Boljša postavitev oznak kanalov, preprečuje, da so drsniki glasnosti "
"neporavnani."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:75
msgid ""
"Maximum latency offset increased from 2 to 5 seconds to accommodate AirPlay "
"devices that often have higher latency than 2 seconds (this is not that "
"useful on newer PulseAudio versions, though, because the latency is reported "
"much more accurately than before)."
msgstr ""
"Največji zamik latence je povečan iz 2 do 5 s za naprave AirPlay, ki imajo "
"pogosto večjo latenco od 2 s (to ni preveč uporabno z novimi različicami "
"PulseAudio, ker se o latenci poroča veliko natančneje kot poprej)."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:76
msgid "New --version command line option."
msgstr "Nova možnost ukazne vrstice --version."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:77
msgid ""
"New translations: Chinese (Taiwan), Croatian, Korean, Norwegian Nynorsk, "
"Lithuanian, Valencian."
msgstr ""
"Novi prevodi: kitajski (Tajvan), hrvaški, korejski, norveški Nynors, "
"litovski, valencijski."

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:78
msgid ""
"Updated translations: Finnish, French, German, Italian, Japanese, Polish, "
"Swedish."
msgstr ""
"Posodobljeni prevodi: finski, francoski, nemški, italijanski, japonski, "
"poljski, švedski."

#: src/org.pulseaudio.pavucontrol.desktop.in:4 src/pavucontrol.cc:877
msgid "PulseAudio Volume Control"
msgstr "Nastavitev glasnosti PulseAudio"

#: src/org.pulseaudio.pavucontrol.desktop.in:6
msgid "Adjust the volume level"
msgstr "Nastavi glasnost"

#: src/org.pulseaudio.pavucontrol.desktop.in:12
msgid ""
"pavucontrol;Microphone;Volume;Fade;Balance;Headset;Speakers;Headphones;Audio;"
"Mixer;Output;Input;Devices;Playback;Recording;System Sounds;Sound Card;"
"Settings;Preferences;"
msgstr ""
"pavucontrol;mikrofon;glasnost;pojemanje;ravnovesje;slušalke;zvočniki;zvočnik;"
"zvok;avdio;mešalnik;izhod;vhod;naprave;predvajanje;snemanje;posnetek;"
"sistemski zvoki;zvočna kartica;nastavitve;lastnosti;prilagodi;prilagajanje;"

#: src/mainwindow.ui:40
msgid "<i>No application is currently playing audio.</i>"
msgstr "<i>Programi trenutno ne predvajajo zvoka.</i>"

#: src/mainwindow.ui:66 src/mainwindow.ui:157
msgid "<b>_Show:</b>"
msgstr "<b>Prik_aži:</b>"

#: src/mainwindow.ui:82 src/mainwindow.ui:173
msgid "All Streams"
msgstr "Vsi pretoki"

#: src/mainwindow.ui:85 src/mainwindow.ui:176
msgid "Applications"
msgstr "Programi"

#: src/mainwindow.ui:88 src/mainwindow.ui:179
msgid "Virtual Streams"
msgstr "Navidezni pretoki"

#: src/mainwindow.ui:107
msgid "_Playback"
msgstr "_Predvajanje"

#: src/mainwindow.ui:131
msgid "<i>No application is currently recording audio.</i>"
msgstr "<i>Programi trenutno ne snemajo zvoka.</i>"

#: src/mainwindow.ui:198
msgid "_Recording"
msgstr "_Snemanje"

#: src/mainwindow.ui:222
msgid "<i>No output devices available</i>"
msgstr "<i>Ni izhodnih naprav</i>"

#: src/mainwindow.ui:248
msgid "<b>S_how:</b>"
msgstr "<b>Pri_kaži:</b>"

#: src/mainwindow.ui:265
msgid "All Output Devices"
msgstr "Vse izhodne naprave"

#: src/mainwindow.ui:268
msgid "Hardware Output Devices"
msgstr "Strojne izhodne naprave"

#: src/mainwindow.ui:271
msgid "Virtual Output Devices"
msgstr "Navidezne izhodne naprave"

#: src/mainwindow.ui:290
msgid "_Output Devices"
msgstr "_Izhodne naprave"

#: src/mainwindow.ui:314
msgid "<i>No input devices available</i>"
msgstr "<i>Ni vhodnih naprav</i>"

#: src/mainwindow.ui:340
msgid "<b>Sho_w:</b>"
msgstr "<b>Prika_ži:</b>"

#: src/mainwindow.ui:357
msgid "All Input Devices"
msgstr "Vse vhodne naprave"

#: src/mainwindow.ui:360
msgid "All Except Monitors"
msgstr "Vsi razen zvočni nadzorniki"

#: src/mainwindow.ui:363
msgid "Hardware Input Devices"
msgstr "Strojne vhodne naprave"

#: src/mainwindow.ui:366
msgid "Virtual Input Devices"
msgstr "Navidezne vhodne naprave"

#: src/mainwindow.ui:369
msgid "Monitors"
msgstr "Zvočni nadzorniki"

#: src/mainwindow.ui:388
msgid "_Input Devices"
msgstr "_Vhodne naprave"

#: src/mainwindow.ui:416
msgid "<i>No cards available for configuration</i>"
msgstr "<i>Ni kartic, primernih za nastavitev</i>"

#: src/mainwindow.ui:431
msgid "Show volume meters"
msgstr "Prikaži merilnike glasnosti"

#: src/mainwindow.ui:448
msgid "_Configuration"
msgstr "_Nastavitve"

#: src/cardwidget.ui:30
msgid "Card Name"
msgstr "Ime kartice"

#: src/cardwidget.ui:45
msgid "Lock card to this profile"
msgstr "Zakleni kartico k temu profilu"

#: src/cardwidget.ui:66
msgid "<b>Profile:</b>"
msgstr "<b>Profil:</b>"

#: src/cardwidget.ui:82
msgid "<b>Codec:</b>"
msgstr "<b>Kodek:</b>"

#: src/channelwidget.ui:9
msgid "<b>left-front</b>"
msgstr "<b>levo-spredaj</b>"

#: src/channelwidget.ui:33
msgid "<small>50%</small>"
msgstr "<small>50%</small>"

#: src/renamedialog.ui:16
msgid "<b>Rename device to:</b>"
msgstr "<b>Preimenuj napravo na:</b>"

#: src/renamedialog.ui:37
msgid "_Cancel"
msgstr "Pre_kliči"

#: src/renamedialog.ui:45
msgid "_Ok"
msgstr "V _redu"

#: src/streamwidget.ui:34
msgid "Stream Title"
msgstr "Naslov pretoka"

#: src/streamwidget.ui:44
msgid "direction"
msgstr "smer"

#: src/streamwidget.ui:64 src/devicewidget.ui:48
msgid "Mute audio"
msgstr "Utišaj zvok"

#: src/streamwidget.ui:78 src/devicewidget.ui:62
msgid "Lock channels together"
msgstr "Skupaj zakleni kanala"

#: src/devicewidget.ui:33
msgid "Device Title"
msgstr "Ime naprave"

#: src/devicewidget.ui:77
msgid "Set as default"
msgstr "Nastavi kot privzeto"

#: src/devicewidget.ui:95
msgid "<b>Port:</b>"
msgstr "<b>Vrata:</b>"

#: src/devicewidget.ui:136
msgid "PCM"
msgstr "PCM"

#: src/devicewidget.ui:148
msgid "AC-3"
msgstr "AC-3"

#: src/devicewidget.ui:158
msgid "DTS"
msgstr "DTS"

#: src/devicewidget.ui:168
msgid "E-AC-3"
msgstr "E-AC-3"

#: src/devicewidget.ui:178
msgid "MPEG"
msgstr "MPEG"

#: src/devicewidget.ui:188
msgid "AAC"
msgstr "AAC"

#: src/devicewidget.ui:198
msgid "TrueHD"
msgstr "TrueHD"

#: src/devicewidget.ui:208
msgid "DTS-HD"
msgstr "DTS-HD"

#: src/devicewidget.ui:224
msgid "<b>Latency offset:</b>"
msgstr "<b>Odmik latence:</b>"

#: src/devicewidget.ui:237
msgid "ms"
msgstr "ms"

#: src/devicewidget.ui:247
msgid "Advanced"
msgstr "Napredno"

#: src/pavucontrol.cc:107
#, c-format
msgid "could not read JSON from list-codecs message response: %s"
msgstr "iz odziva sporočila list-codecs ni možno brati JSON: %s"

#: src/pavucontrol.cc:116
msgid "list-codecs message response is not a JSON array"
msgstr "odziv sporočila list-codecs ni polje JSON"

#: src/pavucontrol.cc:164
msgid "list-codecs message response could not be parsed correctly"
msgstr "sporočila odziva list-codecs ni možno pravilno razčleniti"

#: src/pavucontrol.cc:184
#, c-format
msgid "could not read JSON from get-codec message response: %s"
msgstr "ni možno brati JSON iz sporočilo odziva get-codec: %s"

#: src/pavucontrol.cc:193
msgid "get-codec message response is not a JSON value"
msgstr "sporočilo odziva get-codec ni vrednost JSON"

#: src/pavucontrol.cc:201
msgid "could not get codec name from get-codec message response"
msgstr "imena kodeka ni možno pridobiti iz sporočila odziva get-codec"

#: src/pavucontrol.cc:223
#, c-format
msgid "could not read JSON from get-profile-sticky message response: %s"
msgstr "JSON ni možno brati iz sporočila odziva get-profile-sticky: %s"

#: src/pavucontrol.cc:232
msgid "get-profile-sticky message response is not a JSON value"
msgstr "sporočilo odziva get-profile-sticky ni vrednost JSON"

#: src/pavucontrol.cc:252 src/cardwidget.cc:153 src/cardwidget.cc:181
#, c-format
msgid "pa_context_send_message_to_object() failed: %s"
msgstr "pa_context_send_message_to_object() je spodletel: %s"

#: src/pavucontrol.cc:270
#, c-format
msgid "could not read JSON from list-handlers message response: %s"
msgstr "JSON ni možno brati iz sporočila odziva list-handlers: %s"

#: src/pavucontrol.cc:279
msgid "list-handlers message response is not a JSON array"
msgstr "sporočilo odziva list-handlers ni polje JSON"

#: src/pavucontrol.cc:327
msgid "list-handlers message response could not be parsed correctly"
msgstr "sporočila odziva list-handlers ni možno pravilno razčleniti"

#: src/pavucontrol.cc:361
msgid "Card callback failure"
msgstr "Povratni klic kartice je spodletel"

#: src/pavucontrol.cc:389
msgid "Sink callback failure"
msgstr "Neuspeli povratni klic ponora"

#: src/pavucontrol.cc:413
msgid "Source callback failure"
msgstr "Povratni klic vira je spodletel"

#: src/pavucontrol.cc:432
msgid "Sink input callback failure"
msgstr "Neuspeli povratni klic vhoda ponora"

#: src/pavucontrol.cc:451
msgid "Source output callback failure"
msgstr "Povratni klic izhoda vira je spodletel"

#: src/pavucontrol.cc:481
msgid "Client callback failure"
msgstr "Povratni klic odjemalca je spodletel"

#: src/pavucontrol.cc:497
msgid "Server info callback failure"
msgstr "Povratni klic podatkov strežnika je spodletel"

#: src/pavucontrol.cc:515 src/pavucontrol.cc:812
#, c-format
msgid "Failed to initialize stream_restore extension: %s"
msgstr "Inicializacija razširitve stream_restore je spodletela: %s"

#: src/pavucontrol.cc:533
msgid "pa_ext_stream_restore_read() failed"
msgstr "pa_ext_stream_restore_read() spodletel"

#: src/pavucontrol.cc:551 src/pavucontrol.cc:826
#, c-format
msgid "Failed to initialize device restore extension: %s"
msgstr "Inicializacija razširitve obnovitve naprave je spodletela: %s"

#: src/pavucontrol.cc:572
msgid "pa_ext_device_restore_read_sink_formats() failed"
msgstr "pa_ext_device_restore_read_sink_formats() spodletel"

#: src/pavucontrol.cc:590 src/pavucontrol.cc:839
#, c-format
msgid "Failed to initialize device manager extension: %s"
msgstr "Inicializacija razširitve upravitelja naprave je spodletela: %s"

#: src/pavucontrol.cc:609
msgid "pa_ext_device_manager_read() failed"
msgstr "pa_ext_device_manager_read() spodletel"

#: src/pavucontrol.cc:626
msgid "pa_context_get_sink_info_by_index() failed"
msgstr "pa_context_get_sink_info_by_index() spodletel"

#: src/pavucontrol.cc:639
msgid "pa_context_get_source_info_by_index() failed"
msgstr "pa_context_get_source_info_by_index() spodletel"

#: src/pavucontrol.cc:652 src/pavucontrol.cc:665
msgid "pa_context_get_sink_input_info() failed"
msgstr "pa_context_get_sink_input_info() spodletel"

#: src/pavucontrol.cc:678
msgid "pa_context_get_client_info() failed"
msgstr "pa_context_get_client_info() spodletel"

#: src/pavucontrol.cc:688 src/pavucontrol.cc:753
msgid "pa_context_get_server_info() failed"
msgstr "pa_context_get_server_info() spodletel"

#: src/pavucontrol.cc:701
msgid "pa_context_get_card_info_by_index() failed"
msgstr "pa_context_get_card_info_by_index() spodletel"

#: src/pavucontrol.cc:744
msgid "pa_context_subscribe() failed"
msgstr "pa_context_subscribe() spodletel"

#: src/pavucontrol.cc:760
msgid "pa_context_client_info_list() failed"
msgstr "pa_context_client_info_list() spodletel"

#: src/pavucontrol.cc:767
msgid "pa_context_get_card_info_list() failed"
msgstr "pa_context_get_card_info_list() spodletel"

#: src/pavucontrol.cc:774
msgid "pa_context_get_sink_info_list() failed"
msgstr "pa_context_get_sink_info_list() spodletel"

#: src/pavucontrol.cc:781
msgid "pa_context_get_source_info_list() failed"
msgstr "pa_context_get_source_info_list() spodletel"

#: src/pavucontrol.cc:788
msgid "pa_context_get_sink_input_info_list() failed"
msgstr "pa_context_get_sink_input_info_list() spodletel"

#: src/pavucontrol.cc:795
msgid "pa_context_get_source_output_info_list() failed"
msgstr "pa_context_get_source_output_info_list() spodletel"

#: src/pavucontrol.cc:854 src/pavucontrol.cc:905
msgid "Connection failed, attempting reconnect"
msgstr "Povezava spodletela, poskušam znova"

#: src/pavucontrol.cc:892
msgid ""
"Connection to PulseAudio failed. Automatic retry in 5s\n"
"\n"
"In this case this is likely because PULSE_SERVER in the Environment/X11 Root "
"Window Properties\n"
"or default-server in client.conf is misconfigured.\n"
"This situation can also arise when PulseAudio crashed and left stale details "
"in the X11 Root Window.\n"
"If this is the case, then PulseAudio should autospawn again, or if this is "
"not configured you should\n"
"run start-pulseaudio-x11 manually."
msgstr ""
"Vzpostavitev povezave s storitvijo PulseAudio je spodletela. Znova poskušam "
"čez 5 sekund.\n"
"\n"
"To se lahko pojavi zaradi neustrezno nastavljene spremenljivke PULSE_SERVER "
"v nastavitvah\n"
"Environment/X11 Root Window Properties, ali pa spremenljivke default-server "
"v datoteki client.conf.\n"
"Težava je lahko tudi morebitno predhodno sesutje storitve PulseAudio, ki ni "
"počistila vseh nastavitev\n"
"v oknu X11 Root Window. Če tu ni težave, se bo storitev PulseAudio poskusila "
"zagnati samodejno\n"
"(v redkih primerih pa morate ročno izvesti ukaz start-pulseaudio-x11)."

#: src/cardwidget.cc:126
msgid "pa_context_set_card_profile_by_index() failed"
msgstr "pa_context_set_card_profile_by_index() spodletel"

#: src/channelwidget.cc:99
#, c-format
msgid "<small>%0.0f%% (%0.2f dB)</small>"
msgstr "<small>%0.0f%% (%0.2f dB)</small>"

#: src/channelwidget.cc:101
#, c-format
msgid "<small>%0.0f%% (-&#8734; dB)</small>"
msgstr "<small>%0.0f%% (-&#8734; dB)</small>"

#: src/channelwidget.cc:104
#, c-format
msgid "%0.0f%%"
msgstr "%0.0f%%"

#: src/channelwidget.cc:139
msgid "<small>Silence</small>"
msgstr "<small>Tišina</small>"

#: src/channelwidget.cc:139
msgid "<small>Min</small>"
msgstr "<small>Min</small>"

#: src/channelwidget.cc:141
msgid "<small>100% (0 dB)</small>"
msgstr "<small>100% (0 dB)</small>"

#: src/channelwidget.cc:145
msgid "<small><i>Base</i></small>"
msgstr "<small><i>Osnova</i></small>"

#: src/devicewidget.cc:75
msgid "Rename Device..."
msgstr "Preimenuj napravo ..."

#: src/devicewidget.cc:179
msgid "pa_context_set_port_latency_offset() failed"
msgstr "pa_context_set_port_latency_offset() spodletel"

#: src/devicewidget.cc:257
msgid "Sorry, but device renaming is not supported."
msgstr "Preimenovanje naprav žal ni podprto."

#: src/devicewidget.cc:259
msgid ""
"You need to load module-device-manager in the PulseAudio server in order to "
"rename devices"
msgstr ""
"Za preimenovanje naprav morate naložiti modul module-devices-manager v "
"nastavitvah strežnika PulseAudio"

#: src/devicewidget.cc:294
msgid "pa_ext_device_manager_write() failed"
msgstr "pa_ext_device_manager_write() spodletel"

#: src/mainwindow.cc:171
#, c-format
msgid "Error reading config file %s: %s"
msgstr "Napaka pri branju nastavitvene datoteke %s: %s"

#: src/mainwindow.cc:245
msgid "Error saving preferences"
msgstr "Napaka pri shranjevanju nastavitev"

#: src/mainwindow.cc:253
#, c-format
msgid "Error writing config file %s"
msgstr "Napaka pri pisanju nastavitvene datoteke %s"

#: src/mainwindow.cc:314
msgid " (plugged in)"
msgstr " (vtaknjen)"

#: src/mainwindow.cc:318 src/mainwindow.cc:426
msgid " (unavailable)"
msgstr " (ni na voljo)"

#: src/mainwindow.cc:320 src/mainwindow.cc:423
msgid " (unplugged)"
msgstr " (iztaknjen)"

#: src/mainwindow.cc:625
msgid "Failed to read data from stream"
msgstr "Branje podatkov iz toka je spodletelo"

#: src/mainwindow.cc:669
msgid "Peak detect"
msgstr "Zaznava vrhov"

#: src/mainwindow.cc:670
msgid "Failed to create monitoring stream"
msgstr "Ustvarjanje nadzornega toka je spodletelo"

#: src/mainwindow.cc:685
msgid "Failed to connect monitoring stream"
msgstr "Povezava z nadzornim tokom je spodletela"

#: src/mainwindow.cc:819
msgid ""
"Ignoring sink-input due to it being designated as an event and thus handled "
"by the Event widget"
msgstr "Vhod sink-input je bil prezrt, saj je obravnavan v gradniku Dogodek"

#: src/mainwindow.cc:994
msgid "System Sounds"
msgstr "Sistemski zvoki"

#: src/mainwindow.cc:1340
msgid "Establishing connection to PulseAudio. Please wait..."
msgstr "Vzpostavljam povezavo s storitvijo PulseAudio. Prosimo, počakajte..."

#: src/rolewidget.cc:68
msgid "pa_ext_stream_restore_write() failed"
msgstr "pa_ext_stream_restore_write() spodletel"

#: src/sinkinputwidget.cc:35
msgid "on"
msgstr "na"

#: src/sinkinputwidget.cc:38
msgid "Terminate Playback"
msgstr "Zaustavi predvajanje"

#: src/sinkinputwidget.cc:78
msgid "Unknown output"
msgstr "Neznan izhod"

#: src/sinkinputwidget.cc:87
msgid "pa_context_set_sink_input_volume() failed"
msgstr "pa_context_set_sink_input_volume() spodletel"

#: src/sinkinputwidget.cc:102
msgid "pa_context_set_sink_input_mute() failed"
msgstr "pa_context_set_sink_input_mute() spodletel"

#: src/sinkinputwidget.cc:112
msgid "pa_context_kill_sink_input() failed"
msgstr "pa_context_kill_sink_input() spodletel"

#: src/sinkwidget.cc:119
msgid "pa_context_set_sink_volume_by_index() failed"
msgstr "pa_context_set_sink_volume_by_index() spodletel"

#: src/sinkwidget.cc:137
msgid "Volume Control Feedback Sound"
msgstr "Glasnost povratne zanke"

#: src/sinkwidget.cc:155
msgid "pa_context_set_sink_mute_by_index() failed"
msgstr "pa_context_set_sink_mute_by_index() spodletel"

#: src/sinkwidget.cc:169
msgid "pa_context_set_default_sink() failed"
msgstr "pa_context_set_default_sink() spodletel"

#: src/sinkwidget.cc:189
msgid "pa_context_set_sink_port_by_index() failed"
msgstr "pa_context_set_sink_port_by_index() spodletel"

#: src/sinkwidget.cc:231
msgid "pa_ext_device_restore_save_sink_formats() failed"
msgstr "pa_ext_device_restore_save_sink_formats() spodletel"

#: src/sourceoutputwidget.cc:35
msgid "from"
msgstr "od"

#: src/sourceoutputwidget.cc:38
msgid "Terminate Recording"
msgstr "Zaustavi snemanje"

#: src/sourceoutputwidget.cc:83
msgid "Unknown input"
msgstr "Neznan vhod"

#: src/sourceoutputwidget.cc:93
msgid "pa_context_set_source_output_volume() failed"
msgstr "pa_context_set_source_output_volume() spodletel"

#: src/sourceoutputwidget.cc:108
msgid "pa_context_set_source_output_mute() failed"
msgstr "pa_context_set_source_output_mute() spodletel"

#: src/sourceoutputwidget.cc:119
msgid "pa_context_kill_source_output() failed"
msgstr "pa_context_kill_source_output() spodletel"

#: src/sourcewidget.cc:46
msgid "pa_context_set_source_volume_by_index() failed"
msgstr "pa_context_set_source_volume_by_index() spodletel"

#: src/sourcewidget.cc:61
msgid "pa_context_set_source_mute_by_index() failed"
msgstr "pa_context_set_source_mute_by_index() spodletel"

#: src/sourcewidget.cc:75
msgid "pa_context_set_default_source() failed"
msgstr "pa_context_set_default_source() spodletel"

#: src/sourcewidget.cc:97
msgid "pa_context_set_source_port_by_index() failed"
msgstr "pa_context_set_source_port_by_index() spodletel"

#: src/pavuapplication.cc:158
msgid "Select a specific tab on load."
msgstr "Izberite zavihek ob nalaganju."

#: src/pavuapplication.cc:159
msgid "number"
msgstr "številka"

#: src/pavuapplication.cc:164
msgid "Retry forever if pa quits (every 5 seconds)."
msgstr "Ponavljaj v neskončnost, če se PA zapre (vsakih 5 sekund)."

#: src/pavuapplication.cc:169
msgid "Maximize the window."
msgstr "Razpni okno."

#: src/pavuapplication.cc:174
msgid "Show version."
msgstr "Prikaži različico."

#~ msgid "multimedia-volume-control"
#~ msgstr "večpredstavnost-glasnost-nadzor"

#~ msgid "Terminate"
#~ msgstr "Zaustavi"

#~ msgid "window2"
#~ msgstr "okno2"

#~ msgid "window1"
#~ msgstr "okno1"

#~ msgid "Set as fallback"
#~ msgstr "Nastavi kot zasilni"

#~ msgid "_OK"
#~ msgstr "V _redu"
